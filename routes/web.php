<?php

use App\Http\Controllers\RoleController;
use App\Http\Controllers\ShowRelationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/userl', 'UserController@show');

Route::get('/user/create','UserController@create' );

Route::post('/user', 'UserController@check');

Route::get('/edit/create', 'RoleController@create' );

Route::post('/edit','RoleController@store');

Route::get('/tagl','RoleController@show');

Route::get('/relation', 'ShowRelationController@show');

Route::post('/show_relation','ShowRelationController@check');
