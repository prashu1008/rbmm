<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add User Relationship</title>

 <!-- Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

 <style>
     .name{
        font-size:20px;
        color: #0b2430;

     }
    html, body {
        background-color: rgb(255, 255, 255);
        color: #636b6f;
        font-family: 'Nunito', sans-serif;

        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
    .form-inline {
  display: flex;
  flex-flow: row wrap;
  align-items: center;
    }
  .form-inline label {
  margin: 5px 10px 5px 0;
    }
    .form-inline input {
  vertical-align: middle;
  margin: 5px 10px 5px 0;
  padding: 10px;
  background-color: #fff;
  border: 1px solid #ddd;
}
.form-inline button {
  padding: 10px 20px;
  background-color: rgb(94, 107, 121);
  border: 1px solid #ddd;
  color: white;
}
.form-inline button:hover {
  background-color: rgb(149, 157, 180);
}
@media (max-width: 800px) {
  .form-inline input {
    margin: 10px 0;
  }
  .form-inline {
    flex-direction: column;
    align-items: stretch;
  }
}





</style>

</head>
<body>

    <div class=" flex-center position-ref full-height">

        <div class="content">
            <div class="title m-b-md">
               Show User Relationship
            </div>


    <div class="top-right links">

        <a href="{{ url('/') }}">Home</a></div>

            <form class="form-inline" action="/show_relation" method="post">
                {{ csrf_field() }}

                    <label for="name1">FIRST PERSON NAME:</label>
                    <div class="form-inline">
                <input type="text" id="name1" name="name1" placeholder="Enter Name" required></div>




                <label for="name2">SECOND PERSON NAME:</label>
                <div class="form-inline">
                <input type="text" id="name2" name="name2" placeholder="Enter Name" required></div>

                <div class="form-inline">
                <button type="submit" id= "add_user_relationship_button" >SHOW</button></div>

            </form>
        </div>

    </div>

    <script>
        var msg = '{{Session::get('alert')}}';
        var exist = '{{Session::has('alert')}}';
        if(exist){
          alert(msg);
        }
      </script>

</body>
</html>



