


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User List</title>

 <!-- Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

 <style>
     .name{
        font-size:20px;
        color: #0b2430;

        text-align: left;
     }
    html, body {
        background-color: rgb(255, 255, 255);
        color: #636b6f;
        font-family: 'Nunito', sans-serif;

        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
</style>

</head>

<body>

    <div class=" flex-center position-ref full-height">

        <div class="content">
            <div class="title m-b-md">
                USERS LIST
            </div>
            <table class="table">

                <tr>

    <div class="top-right links">

        <a href="{{ url('/') }}">Home</a></div>

    <tbody>

    @foreach($users as $user)

        <tr>

            <th scope="col">=></th>
            <td ><div class="name">{{$user->name}}</div></td>

        </tr>


    @endforeach
</div>
    </tbody>

</table>
</div>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>
</body>
</html>


