<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Str;


use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;
use Symfony\Component\Console\Input\Input;


class ShowRelationController extends Controller
{
    public function show(){
        return view('/relation');
    }
    public function show_relation($data){

        return view('/show_relation',['datas'=>$data]);



    }
    public function check(Request $request){
        $roles=new Role;
        $name1=$request->input('name1');
        $name2=$request->input('name2');
        $isExistname1 = Role::select("*")
        ->where("name1", $name1,)
        ->exists();

        $isExistname2 = Role::select("*")
        ->where("name2", $name2,)
        ->exists();
if ($isExistname1&&$isExistname2) {
return $this->entry($name1,$name2);
}
else{
    if($isExistname1==false&&$isExistname2==true)
    return redirect()->back() ->with('alert', 'First Person do not exist');
    if($isExistname2==false&&$isExistname1==true)
    return redirect()->back() ->with('alert', 'Second Person do not exist');

   else if($isExistname1==false&&$isExistname2==false){
  return redirect()->back() ->with('alert', 'Enter Correct names From Relations List');
    }
    }
    }
    public function entry($name1,$name2){

        $roles=new Role;
        $count=Role::get()->count();

    $data=[];
 //

     // dd($user1);
   $i=1;
   while ( $i <=$count ) {
      // dd('hello');

       while($name2!=$name1){
           if($u = Role::where('name1', '=', $name1)->first())
           {
              // do something with $u

    $user1= DB::table('roles')->where('name1', $name1)->first();
       // dd($user1);
    $data[$i]=$user1;
    $i++;
   // dd($data);
    $name1=$user1->name2;
      }

     else {
      return 'NO connections between these two people';
    }
};

     return $this->show_relation($data);




   };

}
}


