<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;



class RoleController extends Controller
{

    public function show()
    {
        //DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
       // $users = DB::select('select * from users');
       $roles = Role::all();
        return view('tag', ['roles' => $roles]);
    }
    public function create()
    {
        $roles = new Role;
       return view('edit_tag');

    }
    public function store(Request $request){
      $error=0;
        $roles =new Role();
       // $roles->name1=$request->content["name1"];
       // $roles->role=$request->content["role"];
       // $roles->name2=$request->content["userName2"];

       // if($roles->name1->isEmpty()){
            // has no records
        //    $error++;
       // }
    //else
    try {
    Role::create($request->all());
    return redirect()->back() ->with('alert', 'Relationship Added');
    } catch (\Illuminate\Database\QueryException $e){
        $errorCode = $e->errorInfo[1];
        if($errorCode == 1062){
            return redirect()->back() ->with('alert', 'User Is Already Added');
        }
    }

    //return redirect('/');
    }
}
