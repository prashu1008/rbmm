<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function show()
    {
      //  foreach (range('A', 'Z') as $char){

        //DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
       // $users = DB::select('select * from users');
       $users = User::all();
        return view('user', ['users' => $users]);
    }

    public function create()
    {

        //$user= new User();
        //$user->username= $request['username'];

    // add other fields


       // $users=User::create(['name'=>'john']);

       return view('add_user');
       //user::create($request->all());
    }

    public function store($name){


    DB::insert('insert into users (name) values (?)',[$name]);
    return redirect()->back() ->with('alert', 'User added');
    //return redirect('/');
    }

    public function check(Request $request){
        $users=new User;
        $name=$request->input('name');

        $isExistname = User::select("*")
        ->where("name", $name)
        ->exists();


if (!$isExistname) {
return $this->store($name);
}
else{

    return redirect()->back() ->with('alert', 'User Is Already Present');


    }
    }


}


